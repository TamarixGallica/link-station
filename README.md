# Find the optimal link station for a device placed at given coordinates

This program finds the link station which is able to provide most power for a device placed at given coordinates (if any).

The program takes an array of points where devices are to be placed and an array of link stations which can be used to power the devices. Each point has x and y coordinates. Each link station has x and y coordinates and reach.

The amount of power available from link station to device is calculated with formula `power = (reach - device's distance from link station)^2`. If distance is greater than station's reach, `power = 0`.

Currently the program uses the following link stations:

| X coordinate | Y coordinate | Reach |
| :--- | :--- | :--- |
| 0 | 0 | 10 |
| 20 | 20 | 5 |
| 10 | 0 | 12 |

Available power is calculated for the following points:

| X coordinate | Y coordinate |
| :--- | :--- |
| 0 | 0 |
| 100 | 100 |
| 15 | 10 |
| 18 | 18 |

These values can be changed as needed by modifying `linkstations` and `points` variables in `index.js`.

If none of the link stations is able to power the device, the following message is printed: `No link station within reach for point x,y`. Otherwise the following message is printed: `Best link station for point x,y is x,y with power z`.

## Solution structure

Directory `services` contains services which are used to calculate the distance between two locations and calculate the amount of power available. Directory `tests` contains unit tests for the services.

## Setup

Program requires a [currently supported version](https://nodejs.org/en/about/releases/) of [Node](https://nodejs.org/en/) to run. Version 14 is confirmed to work.

Clone the repository and run `npm install`.

## How to run

Run the program by running `./index.js` or `npm start`.

Tests can be run by running `npm test`. Additionally `npm run test:watch` may be useful when tests should be run after every change.
