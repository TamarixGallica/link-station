const power = (reach, distance) => {
    if (Number.isFinite(reach) && reach >= 0 && Number.isFinite(distance) && distance >= 0) {
        if (distance > reach) {
            return 0;
        }
        return (reach - distance) ** 2;
    };
};

module.exports = power;