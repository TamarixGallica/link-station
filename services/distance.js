const checkArgs = (location) => {
    return location && Number.isFinite(location.x) && Number.isFinite(location.y);
}

const distance = (location1, location2) => {
    if (checkArgs(location1) && checkArgs(location2)) {
        return  (
            Math.sqrt(
                (location2.x - location1.x) ** 2 +
                (location2.y - location1.y) ** 2)
            );
    }
}

module.exports = distance;