const { expect } = require('chai');
const power = require('../services/power');

describe('Power', () => {
    describe('doesn\'t process calls with invalid args', () => {
        it('reach undefined and distance undefined', () => {
            expect(power(undefined, undefined)).to.equal(undefined);
        });

        it('reach String and distance 0', () => {
            expect(power("10", 0)).to.equal(undefined);
        });

        it('reach 1 and distance Array', () => {
            expect(power(1, [1, 3])).to.equal(undefined);
        });

        it('reach -2 and distance 3', () => {
            expect(power(-2, 3)).to.equal(undefined);
        });
    });

    describe('returns expected results with valid reach and distance', () => {
        const testData = [
            { "reach": 1, "distance": 3, result: 0 },
            { "reach": 0, "distance": 0, result: 0 },
            { "reach": 10, "distance": 7, result: 9 },
        ];

        testData.forEach(({reach, distance, result}) => {
            it(`returns ${result} with reach ${reach} and distance ${distance}`, () => {
                expect(power(reach, distance)).to.equal(result)
            });
        });
    });
    
});