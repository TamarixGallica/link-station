const { expect } = require('chai');
const distance = require('../services/distance');

describe('Distance', () => {
    describe('doesn\'t process calls with undefined args', () => {
        it('location1 undefined and location2 undefined', () => {
            expect(distance(undefined, undefined)).to.equal(undefined);
        })

        it('location1 [0, 1] and location2 undefined', () => {
            const location1 = { "x": 0, "y": 1};
            expect(distance(location1, undefined)).to.equal(undefined);
        })

        it('location1 undefined and location2 [3, 4]', () => {
            const location2 = { "x": 3, "y": 4};
            expect(distance(undefined, location2)).to.equal(undefined);
        })

        it('location1 [0, undefined] and location2 undefined', () => {
            const location1 = { "x": 0 };
            expect(distance(location1, undefined)).to.equal(undefined);
        })

        it('location1 undefined and location2 [undefined, 4]', () => {
            const location2 = { "y": 4 };
            expect(distance(undefined, location2)).to.equal(undefined);
        })

        it('location1 [0, undefined] and location2 [undefined, 4]', () => {
            const location1 = { "x": 0 };
            const location2 = { "y": 4 };
            expect(distance(location1, location2)).to.equal(undefined);
        })

        it('location1 [0, 0] and location2 [undefined, 4]', () => {
            const location1 = { "x": 0, "y": 0 };
            const location2 = { "y": 4 };
            expect(distance(location1, location2)).to.equal(undefined);
        })
    });

    describe('doesn\'t process calls with args in incorrect format', () => {
        it('location1 [0, "mouse"] and location2 [undefined, 4]', () => {
            const location1 = { "x": 0, "y": "mouse" };
            const location2 = { "x": 1, "y": 4 };
            expect(distance(location1, location2)).to.equal(undefined);
        })

        it('location1 [0, 0] and location2 [Array, 4]', () => {
            const location1 = { "x": 0, "y": 0 };
            const location2 = { "x": [ 1, 2, 3 ], "y": 4 };
            expect(distance(location1, location2)).to.equal(undefined);
        })
    });

    describe('returns expected results with valid locations', () => {
        const testData = [
            { "location1": { "x": 1, "y": 1}, "location2": { "x": 1, "y": 1}, "result": 0 },
            { "location1": { "x": 0, "y": 1}, "location2": { "x": 0, "y": 1}, "result": 0 },
            { "location1": { "x": 0, "y": 0}, "location2": { "x": 1, "y": 1}, "result": Math.sqrt(2) },
            { "location1": { "x": 1, "y": 0}, "location2": { "x": 2, "y": 3}, "result": Math.sqrt(10) },
            { "location1": { "x": 2, "y": 3}, "location2": { "x": 9, "y": 5}, "result": Math.sqrt(53) },
        ];

        testData.forEach(({location1, location2, result}) => {
            it(`returns ${result} with locations [${location1.x}, ${location1.y}] and [${location2.x}, ${location2.y}]`, () => {
                expect(distance(location1, location2)).to.equal(result)
            });
        });
    });
});
