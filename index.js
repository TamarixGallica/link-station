#!/usr/bin/env node

const distance = require('./services/distance');
const power = require('./services/power');

const linkstations = [
    {
        "x": 0,
        "y": 0,
        "reach": 10,
    },
    {
        "x": 20,
        "y": 20,
        "reach": 5,
    },
    {
        "x": 10,
        "y": 0,
        "reach": 12,
    },
];

const points = [
    {
        "x": 0,
        "y": 0,
    },
    {
        "x": 100,
        "y": 100,
    },
    {
        "x": 15,
        "y": 10,
    },
    {
        "x": 18,
        "y": 18,
    },
];

points.forEach((point) => {
    const powerPerStation = linkstations.map((station) => {
        return {
            ...station,
            power: power(station.reach, distance(point, station)),
        };
    });
    
    if (powerPerStation.every((station) => station.power == 0)) {
        console.log(`No link station within reach for point ${point.x},${point.y}`);
    } else {
        const bestStation = powerPerStation.reduce((prev, cur) => prev.power > cur.power ? prev : cur);
        console.log(`Best link station for point ${point.x},${point.y} is ${bestStation.x},${bestStation.y} with power ${bestStation.power}`);
    }
});